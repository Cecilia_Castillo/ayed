#include <iostream>
#include "graph.h"

using namespace std;

int main() {
    // owo
    GrafoTipo<string> x(8);
    x.AgregarVertice("N2");
    x.AgregarVertice("NOx");
    x.AgregarVertice("Productor");
    x.AgregarVertice("Cons vivo");
    x.AgregarVertice("Cons muerto");
    x.AgregarVertice("NO3-");
    x.AgregarVertice("NO2-");
    x.AgregarVertice("NH4+");
    
	x.AgregarArista("N2", "NO3-", 290);
    x.AgregarArista("N2", "NO3-", 20);
    x.AgregarArista("N2", "NH4+", 160);
    x.AgregarArista("NOx", "NO3-", 320);
    x.AgregarArista("Productor", "Cons vivo", 150);
    x.AgregarArista("Cons vivo", "Cons muerto", 90); // !
    x.AgregarArista("Cons vivo", "NH4+", 100);
    x.AgregarArista("Cons muerto", "NH4+", 50);
    x.AgregarArista("Productor", "NH4+", 50);
    x.AgregarArista("NH4+", "NO2-", 230);
    x.AgregarArista("NO2-", "NO3-", 230);
    x.AgregarArista("NO3-", "Productor", 200);
    x.AgregarArista("NO3-", "N2", 290);


    cout << "grafo x" << endl;
    cout << "tamano: " << x.TamanoGrafo() << endl;
    cout << "vertices: ";
    x.ImprimirVertices();
    cout << endl;
    cout << "aristas: ";
    x.ImprimirAristas();

    x.dijkstra(0);

    cout << endl;

    return 0;
}
