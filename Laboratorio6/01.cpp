#include <iostream>
//primero solicitar al usuario un número de busqueda V y hacer un arreglo de enteros
//positivos ordenados. Para comparar todos los pares de valores es posible hacer un for anidado
//para ir comparando cada número con cada número y ver si es que la suma de esos es igual al número que se busca.
using namespace std;

int main() {
	int n = 10;
	int arr[n] = {1, 5, 10, 12, 16, 20, 41, 60, 61, 85, 100};

	cout << "Se tienen los siguientes " << n << " números:" << endl;
	for (int i = 0; i < n; i++) {
		cout << arr[i] << "  ";
	}

	cout << endl;

	int V = 0;
	cout << "Ingrese un entero positivo V para buscar: ";
	cin >> V;

	bool encontrado = false;

	// búsqueda ineficiente, fuerza bruta
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			int x = arr[i];
			int y = arr[j];
			if (x + y == V) {
				cout << "Los números " << x << " e " << y << " suman " << V << endl;
				encontrado = true;
			}
		}
	}

	if (!encontrado) {
		cout << "No hay ninguna combinación de números que resulte en " << V << endl;
	}

	return 0;
}
