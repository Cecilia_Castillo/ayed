#include <iostream>
#include "mergesort.h"

using namespace std;

int random(int min, int max) {
	// https://stackoverflow.com/a/12658029
	return rand() % (max - min + 1) + min;
}

void swap(int *uno, int *dos){
    int aux = *uno;
    *uno = *dos;
    *dos = aux;
}

void mostrarVector(int arr[], int n){
    for(int i=0; i< n; i++)
        cout << arr[i] << " ";
    cout << "\n";
}

void bubblesort(int arr[], int n) {
	for (int i = 0; i < n - 1; i++){
        for(int j=0; j < n - i - 1; j++){
            if (arr[j] > arr[j + 1]){
                swap(&arr[j], &arr[j+1]);
            }
        }
    }
}

int main() {
	int n = 10;
	cout << "Ingrese el tamaño del arreglo: ";
	cin >> n;

	if (n < 1) {
		cout << "Error: no tiene sentido tener menos de 1 elemento en el arreglo" << endl;
		return 0;
	}

	int arr[n];

	cout << endl << "Se va a llenar de numeros aleatorios" << endl;

	// esto cambia los numeros de rand()
	srand(time(NULL));

	for (int i = 0; i < n; ++i) {
		arr[i] = random(0, 100);
	}

	int menu = 0;

	do {
		cout << "Menu:" << endl;
		cout << " 1. Mostrar datos de arreglo original sin ordenar" << endl;
		cout << " 2. Mostrar datos ordenados mediante algoritmo burbuja" << endl;
		cout << " 3. Mostrar datos ordenados mediante algoritmo mergesort" << endl;
		cout << " 0. Salir" << endl;

		cout << "Ingrese una opción: ";
		cin >> menu;

		if (menu == 1) {
			// Mostrar datos de arreglo original sin ordenar
			cout << "Se tienen los siguientes " << n << " números:" << endl;
			mostrarVector(arr, n);
		}

		else if (menu == 2) {
			int copia[n];

			for (int i = 0; i < n; i++){
				copia[i] = arr[i];
			}

			time_t t_inicio = time(NULL);
			bubblesort(copia, n);
			time_t t_final = time(NULL);

			cout << "ordenado en burbuja:" << endl;
			mostrarVector(copia, n);

			cout << "Tomó " << (t_final - t_inicio) << " miliseg" << endl;
		}

		else if (menu == 3) {
			time_t t_inicio = time(NULL);
			MergeSort ejemplo(n, arr);
			ejemplo.ordenar();
			time_t t_final = time(NULL);
			// usar una pausa de tiempo explícita para visualizar resultados ????

			cout << "ordenado:" << endl;
			ejemplo.mostrarElementos();
			cout << endl << "Tomó " << (t_final - t_inicio) << " miliseg" << endl;
		}
	} while (menu != 0);

	return 0;
}
