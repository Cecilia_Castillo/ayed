#include <iostream>

typedef struct Nodo {
	int nota;
	struct Nodo *izq = nullptr;
	struct Nodo *der = nullptr;
} Nodo;

class Arbol {
	public:
		Nodo* raiz = nullptr;
		Nodo* insertar(Nodo *Nodo, int nota);

	private:
		Nodo* crear_nodo(int nota);
};

Nodo* Arbol::insertar(Nodo *nodo, int nota) {
	if (!nodo) {
		return crear_nodo(nota);
	} else if (nota == nodo->nota) {
		nodo->der = insertar(nodo->der, nota);
	} else if (nota > nodo->nota) {
		nodo->izq = insertar(nodo->izq, nota);
	} else if (nota < nodo->nota) {
		nodo->der = insertar(nodo->der, nota);
	}

	return nodo;
}

Nodo* Arbol::crear_nodo(int nota) {
	Nodo *nodo = new Nodo;
	nodo->nota = nota;
	return nodo;
}

void mostrar_preorden(Nodo *nodo) {
	if (nodo) {
		std::cout << "\t" << nodo->nota << "\t";
		mostrar_preorden(nodo->izq);
		mostrar_preorden(nodo->der);
	}
}

void mostrar_postorden(Nodo *nodo) {
	if (nodo) {
		mostrar_preorden(nodo->izq);
		mostrar_preorden(nodo->der);
		std::cout << "\t" << nodo->nota << "\t";
	}
}

void mostrar_inorden(Nodo *nodo) {
	if (nodo) {
		mostrar_preorden(nodo->izq);
		std::cout << "\t" << nodo->nota << "\t";
		mostrar_preorden(nodo->der);
	}
}

int main() {
	Arbol* arbolito = new Arbol;
	Nodo* raiz = new Nodo;
	bool raiz_creada = false;

	int opcion = 0;

	do {
		std::cout << "1. Insertar numero" << std::endl;
		std::cout << "2. Mostrar en preorden" << std::endl;
		std::cout << "3. Mostrar en postorden" << std::endl;
		std::cout << "4. Mostrar en inorden" << std::endl;
		std::cout << "5. Salir" << std::endl;
		std::cin >> opcion;

		switch (opcion) {
			case 1:
				std::cout << "Ingrese un numero" << std::endl;
				int numero;
				std::cin >> numero;

				if (!raiz_creada) {
					Nodo* nodo = new Nodo;
					nodo->nota = numero;
					arbolito->raiz = nodo;
					raiz = nodo;
					raiz_creada = true;
				} else {
					arbolito->insertar(arbolito->raiz, numero);
				}

				break;
			case 2:
				std::cout << "Preorden: ";
				mostrar_preorden(raiz);
				std::cout << std::endl;
				break;
			case 3:
				std::cout << "Postorden: ";
				mostrar_postorden(raiz);
				std::cout << std::endl;
				break;
			case 4:
				std::cout << "Inorden: ";
				mostrar_inorden(raiz);
				std::cout << std::endl;
				break;
			default:
				std::cout << "Salir" << std::endl;
				opcion = 0;
				break;
		}
	} while (opcion != 0);
		return 0;

	return 0;
}
