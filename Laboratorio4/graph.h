const int ARISTA_NULO = 0;

template <typename T>
class GrafoTipo {
  public:
    GrafoTipo(int);
    ~GrafoTipo();
    void AgregarVertice(T);
    void AgregarArista(T, T, int);
    int PesoEs(T, T);
    int IndexIs(T*, T);
    bool EstaVacio();
    bool EstaLleno();
    int TamanoGrafo();

 private:
    int numVertices;
    int maxVertices;

    T* vertices;
    int **aristas;
};

template <typename T>
GrafoTipo<T>::GrafoTipo(int maxV){
   numVertices = 0;
   maxVertices = maxV;
   vertices = new T[maxV];
   aristas = new int*[maxV];

   for(int i = 0; i < maxV; i++)
     aristas[i] = new int[maxV];
}

template <typename T>
GrafoTipo<T>::~GrafoTipo(){
   delete [] vertices;

   for(int i = 0; i < maxVertices; i++)
      delete [] aristas[i];

   delete [] aristas;
}

template <typename T>
void GrafoTipo<T>::AgregarVertice(T vertice){
   vertices[numVertices] = vertice;

   for(int i = 0; i < numVertices; i++) {
     aristas[numVertices][i] = ARISTA_NULO;
     aristas[i][numVertices] = ARISTA_NULO;
   }

   numVertices++;
}

template <typename T>
void GrafoTipo<T>::AgregarArista(T desdeVertice, T hastaVertice, int peso){
   int fila, col;

   fila = IndexIs(vertices, desdeVertice);
   col = IndexIs(vertices, hastaVertice);
   aristas[fila][col] = peso;
}

template <typename T>
int GrafoTipo<T>::PesoEs(T desdeVertice, T hastaVertice){
   int fila, col;

   fila = IndexIs(vertices, desdeVertice);
   col = IndexIs(vertices, hastaVertice);

   return aristas[fila][col];
}

template <typename T>
int GrafoTipo<T>::IndexIs(T *vertices, T vertice){
  for(int i = 0; i < numVertices; i++){
      if (vertices[i] == vertice)
          return i;
  }

  return -1;
}

template <typename T>
bool GrafoTipo<T>::EstaLleno(){
  if (numVertices == maxVertices)
      return true;

  return false;
}

template <typename T>
bool GrafoTipo<T>::EstaVacio(){
  if (numVertices == 0)
      return true;

  return false;
}

template <typename T>
int GrafoTipo<T>::TamanoGrafo(){
  return numVertices;
}
