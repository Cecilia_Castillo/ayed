#include <limits.h>

const int ARISTA_NULO = 0;

using namespace std;

template <typename T>
class GrafoTipo {
public:
    GrafoTipo(int);
    ~GrafoTipo();
    void AgregarVertice(T);
    void AgregarArista(T, T, int);
    int PesoEs(T, T);
    int IndexIs(T);
    bool EstaVacio();
    bool EstaLleno();
    int TamanoGrafo();
    void ImprimirVertices();
    void ImprimirAristas();

    int minDistance(int*, bool*);
    void dijkstra(int);

private:
    int numVertices;
    int maxVertices;

    T* vertices;
    int** aristas; // matriz de adyacencia
};

template <typename T>
GrafoTipo<T>::GrafoTipo(int maxV){
    numVertices = 0;
    maxVertices = maxV;
    vertices = new T[maxV];
    aristas = new int*[maxV];  // peso = int

    for(int i = 0; i < maxV; i++)
        aristas[i] = new int[maxV];
}

template <typename T>
GrafoTipo<T>::~GrafoTipo(){
    delete [] vertices;

    for(int i = 0; i < maxVertices; i++)
        delete [] aristas[i];

    delete [] aristas;
}

template <typename T>
void GrafoTipo<T>::AgregarVertice(T vertice){
    vertices[numVertices] = vertice;

    for (int i = 0; i < numVertices; i++) {
        aristas[numVertices][i] = ARISTA_NULO;
        aristas[i][numVertices] = ARISTA_NULO;
    }

    numVertices++;
}

template <typename T>
void GrafoTipo<T>::AgregarArista(T desdeVertice, T hastaVertice, int peso){
    int fila = IndexIs(desdeVertice);
    int col = IndexIs(hastaVertice);
    aristas[fila][col] = peso;
}

template <typename T>
int GrafoTipo<T>::PesoEs(T desdeVertice, T hastaVertice){
    int fila = IndexIs(desdeVertice);
    int col = IndexIs(hastaVertice);

    return aristas[fila][col];
}

template <typename T>
int GrafoTipo<T>::IndexIs(T vertice){
    for (int i = 0; i < numVertices; i++){
        if (vertices[i] == vertice)
            return i;
    }

    return -1;
}

template <typename T>
bool GrafoTipo<T>::EstaLleno(){
    if (numVertices == maxVertices)
        return true;

    return false;
}

template <typename T>
bool GrafoTipo<T>::EstaVacio(){
    if (numVertices == 0)
        return true;

    return false;
}

template <typename T>
int GrafoTipo<T>::TamanoGrafo(){
    return numVertices;
}

template <typename T>
void GrafoTipo<T>::ImprimirVertices() {
    for (int i = 0; i < numVertices; i++)
        cout << vertices[i] << "  ";

    cout << endl;
}

template <typename T>
void GrafoTipo<T>::ImprimirAristas() {
    cout << endl;

    for (int i = 0; i < maxVertices; i++) {
        for (int j = 0; j < maxVertices; j++)
            cout << aristas[i][j] << '\t';

        cout << endl;
    }
}

template <typename T>
int GrafoTipo<T>::minDistance(int dist[], bool sptSet[]) {
    int V = maxVertices;
    // Inicializar valor mínim
    int min = INT_MAX, min_index;

    for (int v = 0; v < V; v++)
        if (sptSet[v] == false && dist[v] <= min)
            min = dist[v], min_index = v;

    return min_index;
}

// src = Indice del vertice origen
template <typename T>
void GrafoTipo<T>::dijkstra(int src) {
    int V = maxVertices;

    if (src > V)
        return;

    int dist[V]; // The output array.  dist[i] will hold the shortest
    // distance from src to i

    bool sptSet[V]; // sptSet[i] will be true if vertex i is included in shortest
    // path tree or shortest distance from src to i is finalized

    // Initialize all distances as INFINITE and stpSet[] as false
    for (int i = 0; i < V; i++) {
        dist[i] = INT_MAX;
        sptSet[i] = false;
    }

    // Distance of source vertex from itself is always 0
    dist[src] = 0;

    // Find shortest path for all vertices
    for (int count = 0; count < V - 1; count++) {
        // Pick the minimum distance vertex from the set of vertices not
        // yet processed. u is always equal to src in the first iteration.
        int u = minDistance(dist, sptSet);

        // Mark the picked vertex as processed
        sptSet[u] = true;

        // Update dist value of the adjacent vertices of the picked vertex.
        for (int v = 0; v < V; v++)

            // Update dist[v] only if is not in sptSet, there is an edge from
            // u to v, and total weight of path from src to  v through u is
            // smaller than current value of dist[v]
            if (!sptSet[v] && aristas[u][v] && dist[u] != INT_MAX
                && dist[u] + aristas[u][v] < dist[v])
                dist[v] = dist[u] + aristas[u][v];
    }

    // print the constructed distance array
    printf("Vertex   Distance from Source\n");
    for (int i = 0; i < V; i++)
        cout << vertices[i] << " \t\t " << dist[i] << endl;
}
