#include <iostream>
#include "graph.h"

using namespace std;

int main() {
    GrafoTipo<float> x(10.1);

    cout << "Vacio? " << x.EstaVacio() << " - " << x.TamanoGrafo() << endl;


    x.AgregarVertice(10.1);
    x.AgregarVertice(20.1);
    cout << "\nVacio? " << x.EstaVacio() << " - " << x.TamanoGrafo() << endl;
    x.AgregarArista(10.1, 20.1, 100);
    cout << "Peso: " << x.PesoEs(10.1, 20.1) << endl;
    x.AgregarVertice(30.1);
    cout << "\nVaciÃ³? " << x.EstaVacio() << " - " << x.TamanoGrafo() << endl;
    x.AgregarArista(10.1, 30.1, 200);
    cout << "Peso: " << x.PesoEs(10.1, 30.1) << endl;

    x.AgregarVertice(40.1);
    cout << "\nVaciÃ³? " << x.EstaVacio() << " - " << x.TamanoGrafo() << endl;
    x.AgregarArista(10.1, 40.1, 600);
    cout << "Peso: " << x.PesoEs(10.1, 40.1) << endl;


    cout << "\n" << endl;

    GrafoTipo<int> y(10);

    cout << "Vacio? " << y.EstaVacio() << " - " << y.TamanoGrafo() << endl;

    y.AgregarVertice(10);
    y.AgregarVertice(20);
    cout << "\nVacio? " << y.EstaVacio() << " - " << y.TamanoGrafo() << endl;
    y.AgregarArista(10, 20, 100);
    cout << "Peso: " << y.PesoEs(10, 20) << endl;
    y.AgregarVertice(30);
    cout << "\nVacio? " << y.EstaVacio() << " - " << y.TamanoGrafo() << endl;
    y.AgregarArista(10, 30, 200);
    cout << "Peso: " << y.PesoEs(10, 30) << endl;

    y.AgregarVertice(40);
    cout << "\nVacio? " << y.EstaVacio() << " - " << y.TamanoGrafo() << endl;
    y.AgregarArista(10, 40, 600);
    cout << "Peso: " << y.PesoEs(10, 40) << endl;
}
