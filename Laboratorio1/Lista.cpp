#include<iostream>
#include<string>

using namespace std;

class Estudiante {
    private: 
        string nombre;
        int edad;

    public:
        Estudiante(string nombre, int edad){
            this->nombre = nombre;
            this->edad = edad;
        }

        Estudiante(){
            this->nombre = "";
            this->edad = 0;
        }

        string getNombre(){
            return nombre;
        }

        void setNombre(string nombre){
            this->nombre = nombre;
        }   

        int getEdad(){
            return edad;
        }

        void setEdad(int edad){
            this->edad = edad;
        }
};


class Nodo {
    private:
        Estudiante valor;
        Nodo *vecino;

    public:
        Nodo(){
            this->vecino = NULL;
        }

        Nodo(Estudiante nuevo){
            this->valor.setNombre(nuevo.getNombre());
            this->valor.setEdad(nuevo.getEdad());

            this->vecino = NULL;
        }

        void setVecino(Nodo *vec){
            this->vecino = vec;
        }
                
        Nodo *getVecino(){
            return(this->vecino);
        }

        Estudiante *getEstudiante(){
            return(&this->valor);
        }

};

class Lista {
    private:
        Nodo *primero, *ultimo;

    public:
        Lista(){
            primero = ultimo = NULL;
        }

        void insertarNodo(Nodo *nuevo){
            if (ultimo == NULL){
                primero = ultimo = nuevo;
            }else{
                ultimo->setVecino(nuevo);
                ultimo = nuevo;
            }            
        }

        void mostrarLista(){
            Nodo *recorrer;

            recorrer = primero;

            while(recorrer!=NULL){
                Estudiante *estudiante;
                estudiante = recorrer->getEstudiante();

                cout <<"Estudiante" << endl;
                cout <<"-- Nombre: " << estudiante->getNombre() << endl;
                cout <<"-- Edad: " << estudiante->getEdad() << endl;

                recorrer = recorrer->getVecino();
           }
        }
        
        bool buscarEstudiante(string nombre){
        	Nodo *recorrer;
        	
        	recorrer = primero;
        	
			while(recorrer != NULL){
				if (recorrer->getEstudiante()->getNombre() == nombre) {
					// encontrado
					return true;
				}
				
				// si no se encontro ir al siguiente nodo
        		recorrer = recorrer->getVecino();
			}
			
			// si no encontramos en ningun nodo entonces el estudiante no está
			return false;
		}
		
		// esta operación permite agregar un nodo nuevo como primer nodo de la lista enlazada actual
		// (el que antes era primero, ahora es segundo)
		void insertarPrimero(Nodo *nodo) {
			nodo->setVecino(primero);
			primero = nodo;
		}
		
		void quitarUltimo(){
			Nodo *recorrer;
        	
        	recorrer = primero;
        	
			while(recorrer != NULL){
				// avanzar hasta encontrar el penultimo
				// el penultimo nodo es el que el vecino es el ultimo
				if (recorrer->getVecino() == ultimo) {
					recorrer->setVecino(NULL);
					ultimo = NULL;
					return;
				}
				
        		recorrer = recorrer->getVecino();
			}
		}
		
		void quitarPrimero() {
			primero = primero->getVecino();
		}
};

int main(){    
    Lista lista;
    Estudiante p("Estudiante 1", 20);
    Nodo *nuevo= new Nodo(p);
    lista.insertarNodo(nuevo);

    Estudiante q("Estudiante 2", 38);
	Nodo *nuevo2= new Nodo(q);
    lista.insertarNodo(nuevo2);
    
    Estudiante f("Estudiante 3", 30);
	Nodo *nuevo3= new Nodo(f);
	lista.insertarNodo(nuevo3);
	
	cout << "Lista en estado inicial con 3 estudiantes" << endl;
	lista.mostrarLista();

	cout << "Intentando buscar al Estudiante 3" << endl;
	if (lista.buscarEstudiante("Estudiante 3")){
		cout << "El estudiante 3 si existe" << endl;
	} else {
		cout << "El estudiante no existe" << endl;
	}
	
	if (lista.buscarEstudiante("Estudiante 684645")){
		cout << "El estudiante 684645 si existe" << endl;
	} else {
		cout << "El estudiante 684645 no existe" << endl;
	}

	cout << endl << endl;

	Estudiante h("Estudiante Nuevo 1", 10);
	Nodo *nuevo4 = new Nodo(h);
	
	cout << "Primero se elimina al primer estudiante y se pone otro primer estudiante en su lugar:" << endl;
	lista.quitarPrimero();
	lista.insertarPrimero(nuevo4);
	lista.mostrarLista();

	cout << endl << endl;
	
	cout << "Ahora se borra el ultimo estudiante" << endl;
	lista.quitarUltimo();
	lista.mostrarLista();

    return 0;
}
