#include<vector>

using namespace std;

class MergeSort{
    public:
        MergeSort(int, int[]);
        void ordenar();
        void mostrarElementos();

    private:
        int tamanno;
        vector<int> datos;
        void ordenarSubArreglo(int, int);
        void mezclar(int, int, int, int);
        void mostrarSubArreglo(int, int);
};
